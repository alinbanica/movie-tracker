class Review < ActiveRecord::Base
  include CommonScopes
  include CommonMethods

  belongs_to :movie
  belongs_to :user

  validates :content, presence: true

  before_update :do_not_update_fields, if: :changed?

  private

  def unchangeable_fields
    [:user_id, :movie_id]
  end
end