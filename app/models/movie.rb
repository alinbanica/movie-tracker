class Movie < ActiveRecord::Base
  include CommonMethods

  has_many :watched_movies, :dependent => :destroy
  has_many :movies, through: :watched_movies

  has_many :reviews, :dependent => :destroy

  validates :title, :release_date, :runtime, presence: true
  validates :runtime, numericality: { only_integer: true, greater_than: 0 }

  def as_json(options = {})
    hash = super
    hash['release_date'] = hash['release_date'].iso8601
    hash
  end
end