class User < ActiveRecord::Base
  include CommonMethods

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :watched_movies, :dependent => :destroy
  has_many :movies, through: :watched_movies

  has_many :reviews, :dependent => :destroy

  ADMIN_USER = 'admin'
  BASIC_USER = 'user'

  USER_ROLES = [ADMIN_USER, BASIC_USER]

  before_create :generate_authentication_token

  validates :authentication_token, uniqueness: true
  validates :role, presence: true
  validates :role, inclusion: { in: USER_ROLES }

  scope :admins, -> {where(:role => ADMIN_USER)}

  def is_admin_user?
    has_role? && is?(ADMIN_USER)
  end

  def is_basic_user?
    has_role? && is?(BASIC_USER)
  end

  private

  def is?(checked_role)
    role == checked_role
  end

  def has_role?
    role.present? && USER_ROLES.include?(role)
  end

  def generate_authentication_token
    loop do
      self.authentication_token = Devise.friendly_token
      break unless User.find_by(authentication_token: authentication_token)
    end
  end
end
