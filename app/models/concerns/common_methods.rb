module CommonMethods
  extend ActiveSupport::Concern

  def as_json(options = {})
    hash = super
    hash['created_at'] = hash['created_at'].iso8601
    hash['updated_at'] = hash['updated_at'].iso8601
    hash
  end

  def do_not_update_fields
    unchangeable_fields.each do |field|
      eval("self.#{field} = self.#{field}_was")
    end
  end
end