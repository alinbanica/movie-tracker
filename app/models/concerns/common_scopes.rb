module CommonScopes
  extend ActiveSupport::Concern

  included do
    validates :user_id, :movie_id, presence: true
    validates :user_id, :movie_id, numericality: { only_integer: true }

    scope :by_user, ->(user_id) { where(user_id: user_id ) }
    scope :by_movie, ->(movie_id) { where(movie_id: movie_id ) }
  end

end