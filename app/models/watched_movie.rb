class WatchedMovie < ActiveRecord::Base
  include CommonScopes
  include CommonMethods

  belongs_to :movie
  belongs_to :user

  RATING_RANGE = 1..10

  validates :rating, presence: true
  validates :rating, inclusion: { in: RATING_RANGE }, if: 'rating.present?'
  validates :movie_id, uniqueness: {scope: :user_id}

  before_update :do_not_update_fields, if: :changed?

  private

  def unchangeable_fields
    [:user_id, :movie_id]
  end
end