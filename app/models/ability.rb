class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new # guest user (not logged in)

    if user.is_admin_user?
      can :manage, :all
    elsif user.is_basic_user?
      can :read, [User, Movie]
      can [:create, :read], [WatchedMovie, Review]
      can [:update, :destroy], [User], :id => user.id
      can [:update, :destroy], [WatchedMovie, Review], :user_id => user.id
    else
      can :create, User
      can :read, :all
    end
  end
end
