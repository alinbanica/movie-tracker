class ApplicationController < ActionController::API
  include CanCan::ControllerAdditions
  include AuthenticationOrRequest

  rescue_from Exception do |exception|
    status = :internal_server_error # 500

    status = :not_found if exception.kind_of? ActiveRecord::RecordNotFound # 404
    status = :bad_request if exception.kind_of? ActiveRecord::RecordInvalid # 400
    status = :forbidden if exception.kind_of? CanCan::AccessDenied # 403

    render_response([exception.message], status)
  end

  private

  def build_success_response(response_data)
    { success: true, data: response_data }
  end

  def build_error_repsonse(response_data)
    { success: false, errors: response_data }
  end

  def render_response(request_response, status = :ok)
    response = status == :ok ? build_success_response(request_response) : build_error_repsonse(request_response)

    render :json => response, :status => status
  end

  def current_user
    @current_user || nil
  end
end
