module Api
  module V1
    class MoviesController < ApplicationController
      load_and_authorize_resource

      def index
        render_response(Movie.all)
      end

      def create
        movie = Movie.new(permitted_params)
        movie.save!
        render_response(movie)
      end

      def show
        render_response(Movie.find(params[:id]))
      end

      def update
        movie = Movie.find(params[:id])
        movie.update!(permitted_params)
        render_response(movie)
      end

      def destroy
        movie = Movie.find(params[:id])
        movie.destroy!
        render_response(nil)
      end

      private

      def permitted_params
        params.permit(:title, :storyline, :release_date, :language, :runtime)
      end
    end
  end
end
