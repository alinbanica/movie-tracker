module Api
  module V1
    class SessionsController < ApplicationController
      skip_before_filter :authenticate_or_request, :only => [:logout]

      def logout
        render_response(["Logout successfull"], :unauthorized)
      end

      def login
        render_response(["Login successfull"], :ok)
      end
    end
  end
end