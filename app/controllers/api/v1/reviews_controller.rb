module Api
  module V1
    class ReviewsController < ApplicationController
      load_and_authorize_resource

      def index
        reviews = Review.all
        reviews = reviews.by_user(params[:user_id]) if params[:user_id].present?
        reviews = reviews.by_movie(params[:movie_id]) if params[:movie_id].present?
        render_response(reviews)
      end

      def create
        review = Review.new(permitted_params)
        review.save!
        render_response(review)
      end

      def show
        render_response(Review.find(params[:id]))
      end

      def update
        review = Review.find(params[:id])
        review.update!(permitted_params)
        render_response(review)
      end

      def destroy
        review = Review.find(params[:id])
        review.destroy!
        render_response(nil)
      end

      private

      def permitted_params
        params.permit(:user_id, :movie_id, :content)
      end
    end
  end
end