module Api
  module V1
    class UsersController < ApplicationController
      load_and_authorize_resource

      skip_before_filter :authenticate_or_request, :only => [:create]

      def index
        render_response(User.all.as_json)
      end

      def create
        user = User.new(permitted_params)

        if user.is_admin_user?
          if current_user.present? && current_user.is_admin_user?
            user_save!
          else
            raise CanCan::AccessDenied, 'You need admin right for this operation'
          end
        else
          user.save!
        end

        render_response(user)
      end

      def show
        render_response(User.find(params[:id]))
      end

      def update
        user = User.find(params[:id])
        user.update!(permitted_params)
        render_response(user)
      end

      def destroy
        user = User.find(params[:id])
        user.destroy!
        render_response(nil)
      end

      private

      def permitted_params
        params.permit(:email, :role, :password)
      end
    end
  end
end
