module Api
  module V1
    class WatchedMoviesController < ApplicationController
      load_and_authorize_resource

      def index
        watched_movies = WatchedMovie.all
        watched_movies = watched_movies.by_user(params[:user_id]) if params[:user_id].present?
        render_response(watched_movies)
      end

      def create
        watched_movie = WatchedMovie.new(permitted_params)
        watched_movie.save!
        render_response(watched_movie)
      end

      def show
        render_response(WatchedMovie.find(params[:id]))
      end

      def update
        movie = WatchedMovie.find(params[:id])
        movie.update!(permitted_params)
        render_response(movie)
      end

      def destroy
        movie = WatchedMovie.find(params[:id])
        movie.destroy!
        render_response(nil)
      end

      private

      def permitted_params
        params.permit(:user_id, :movie_id, :rating)
      end
    end
  end
end