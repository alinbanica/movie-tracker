module AuthenticationOrRequest
  extend ActiveSupport::Concern

  include ActionController::HttpAuthentication::Basic::ControllerMethods
  include ActionController::HttpAuthentication::Token::ControllerMethods

  included do
    before_filter :authenticate_or_request, :except => [:index, :show]
  end

  def authenticate_or_request
    if has_authentication?
      header_infos = request.headers['Authorization'].split(' ')
    else
      header_infos = []
    end

    if header_infos[0] == 'Basic'
      basic_authentication
    elsif header_infos[0] == 'Token'
      token_authentication
    else
      request_authentication
    end
  end

  private

  def basic_authentication
    authenticate_or_request_with_http_basic do |email, password|
      user = User.find_by(email: email)

      if user && user.valid_password?(password)
        @current_user = user
      else
        request_authentication
      end
    end
  end

  def token_authentication
    authenticate_or_request_with_http_token do |token, options|
      user = User.find_by(authentication_token: token)

      if user
        @current_user = user
      else
        request_authentication('Token')
      end
    end
  end

  def request_authentication(type = 'Basic')
    self.headers["WWW-Authenticate"] = "#{type} realm=Application"
    render_response(["#{type} Authentication Failed"], :unauthorized)
  end

  def has_authentication?
    request.headers['Authorization'].present?
  end
end
