Rails.application.routes.draw do
  # devise_for :users
  namespace :api, defaults: { format: :json } do
    namespace :v1 do
      resources :users, :only => [:index, :create, :show, :update, :destroy]
      resources :movies, :only => [:index, :create, :show, :update, :destroy]
      resources :watched_movies, :only => [:index, :create, :show, :update, :destroy]
      resources :reviews, :only => [:index, :create, :show, :update, :destroy]

      get 'login', to: 'sessions#login', as: 'login'
      get 'logout', to: 'sessions#logout', as: 'logout'
    end
  end
end
