# require 'ffaker'
admin_users = []
basic_users = []
movies = []
watched_movies = []

# create 2 admin users
2.times do |i|
  admin_users << FactoryGirl.create(:admin)
end

# create 3 basic users
3.times do |i|
  basic_users << FactoryGirl.create(:user)
end

# create 10 Movies
10.times do |i|
  movies << FactoryGirl.create(:movie)
end

# first admin has 2 movies watched one with rating
watched_movies << FactoryGirl.create(:watched_movie, :user => admin_users[0], :movie => movies[0])
watched_movies << FactoryGirl.create(:watched_movie, :user => admin_users[0], :movie => movies[8], :rating => 8)

# first basic user has one movie watched
watched_movies << FactoryGirl.create(:watched_movie, :user => basic_users[0], :movie => movies[0])

# second basic user has 3 movies watched
watched_movies << FactoryGirl.create(:watched_movie, :user => basic_users[1], :movie => movies[7])
watched_movies << FactoryGirl.create(:watched_movie, :user => basic_users[1], :movie => movies[2])
watched_movies << FactoryGirl.create(:watched_movie, :user => basic_users[1], :movie => movies[3])

# first movie has 3 reviews
FactoryGirl.create(:review, :user => admin_users[0], :movie => movies[0], :content => 'Hello World')
FactoryGirl.create(:review, :user => admin_users[0], :movie => movies[0], :content => 'How are you')
FactoryGirl.create(:review, :user => basic_users[0], :movie => movies[0], :content => 'I am ok')
