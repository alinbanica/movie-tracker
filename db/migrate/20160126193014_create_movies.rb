class CreateMovies < ActiveRecord::Migration
  def change
    create_table :movies do |t|
      t.string :title
      t.text  :storyline
      t.date :release_date
      t.string :language
      t.integer :runtime
      t.timestamps
    end
  end
end
