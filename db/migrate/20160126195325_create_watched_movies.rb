class CreateWatchedMovies < ActiveRecord::Migration
  def change
    create_table :watched_movies do |t|
      t.belongs_to :user, index: true
      t.belongs_to :movie, index: true
      t.integer :rating
      t.timestamps
      t.index [:user_id, :movie_id], unique: true
    end
  end
end
