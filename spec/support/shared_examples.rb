module SharedExamples
  shared_examples_for 'is_require_authentication' do
    it 'should require authentication details' do
      send_request(http_method, action, opts)
      check_error_response(:unauthorized)
    end

    it 'should verify authentication details' do
      basic_authentication('test@example.com', 'test_1234')
      send_request(http_method, action, opts)
      check_error_response(:unauthorized)
    end
  end

  shared_examples_for 'is_not_require_authentication' do
    it 'should not require authentication details' do
      send_request(http_method, action, opts)
      check_success_response
    end

    it 'should verify authentication details' do
      basic_authentication('test@example.com', 'test_1234')
      send_request(http_method, action, opts)
      check_success_response
    end
  end

  shared_examples_for 'is_require_valid_record_id' do |model|
    it 'should require a valid record id'  do
      opts = { :id => model.maximum(:id).to_i.next }
      basic_authentication(user.email, user.password)
      send_request(http_method, action, opts)
      check_error_response(:not_found)
    end
  end

  shared_examples_for 'is_authorized_user' do |authenticate|
    it 'should authorize user action' do
      basic_authentication(user.email, user.password) if authenticate
      send_request(http_method, action, opts)
      check_success_response
    end
  end

  shared_examples_for 'is_not_authorized_user' do |authenticate|
    it 'should not authorize user action' do
      basic_authentication(user.email, user.password) if authenticate
      send_request(http_method, action, opts)
      check_error_response(:forbidden)
    end
  end

  shared_examples_for 'is_authorized_guest_user' do
    it_behaves_like 'is_authorized_user', false
    it_behaves_like 'is_authorized_user', false do
      let(:opts) { {:id => user2.id} }
    end
  end

  shared_examples_for 'is_authorized_basic_user' do
    it_behaves_like 'is_authorized_user', true
    it_behaves_like 'is_not_authorized_user', true do
      let(:opts) { {:id => user2.id} }
    end
  end

  shared_examples_for 'is_authorized_admin_user' do
    it_behaves_like 'is_authorized_user', true
    it_behaves_like 'is_authorized_user', true do
      let(:opts) { {:id => user2.id} }
    end
  end

  shared_examples_for 'can_view_all_records' do |authenticate, model|
    it 'should list all records' do
      basic_authentication(user.email, user.password) if authenticate
      send_request(http_method, action, opts)

      json = check_success_response
      expect(json['data']).to eq(model.all.as_json)
    end
  end

  shared_examples_for 'can_create_new_record' do |authenticate, model|
    it 'should create a new record' do
      cnt = model.all.count
      basic_authentication(user.email, user.password) if authenticate
      send_request(http_method, action, opts)

      json = check_success_response
      expect(json['data']).to eq(model.last.as_json)
      expect(model.all.count).to eq(cnt+1)
    end
  end

  shared_examples_for 'can_view_record_details' do |authenticate, model|
    it 'should show record details' do
      basic_authentication(user.email, user.password) if authenticate
      send_request(http_method, action, opts)

      json = check_success_response
      expect(json['data']).to eq(model.find(opts[:id]).as_json)
    end
  end

  shared_examples_for 'can_update_record' do |authenticate, model|
    it 'should update record details' do
      basic_authentication(user.email, user.password) if authenticate
      send_request(http_method, action, opts)

      json = check_success_response
      expect(json['data']).to eq(model.find(opts[:id]).as_json)
    end
  end

  shared_examples_for 'cannot_update_record' do |authenticate, model|
    it_behaves_like 'is_not_authorized_user', authenticate
  end

  shared_examples_for 'can_delete_record' do |authenticate|
    it 'should delete record details' do
      basic_authentication(user.email, user.password) if authenticate
      send_request(http_method, action, opts)

      json = check_success_response
      expect(json['data']).to be_nil
    end
  end

  shared_examples_for 'cannot_delete_record' do |authenticate, model|
    it_behaves_like 'is_not_authorized_user', authenticate
  end

  shared_examples_for 'invalid_data_on_create' do |text, authenticate|
    it 'should validate record on creation when #{text}' do
      basic_authentication(user.email, user.password) if authenticate
      send_request(http_method, action, opts)
      check_error_response(:bad_request)
    end
  end

  shared_examples_for 'empty_data' do |text, authenticate|
    it "should list no records when #{text}" do
      basic_authentication(user.email, user.password) if authenticate
      send_request(http_method, action, opts)

      json = check_success_response
      expect(json['data']).to be_empty
    end
  end

  shared_examples_for 'invalid_field' do |text, factory_model|
    it "should not be valid when #{text}" do
      expect(FactoryGirl.build(factory_model, ops)).not_to be_valid
    end
  end

  shared_examples_for 'valid_model' do |factory_model|
    it 'should have a valid factory' do
      expect(FactoryGirl.build(factory_model)).to be_valid
    end
  end
end