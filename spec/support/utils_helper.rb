module UtilsHelper
  def http_status(symbol)
    Rack::Utils::SYMBOL_TO_STATUS_CODE[symbol]
  end

  def check_error_response(type)
    expect(response.status).to eq(http_status(type))

    json = JSON.parse(response.body)

    expect(json['success']).to be false
    expect(json['errors']).to_not be_empty

    json
  end

  def check_success_response
    expect(response.status).to eq(http_status(:ok))

    json = JSON.parse(response.body)

    expect(json['success']).to be true

    json
  end

  def send_request(http_method, action, options = {}, format = :json)
    options.merge!(:format => format)

    case http_method
    when :post
      post action, options
    when :put
      put action, options
    when :get
      get action, options
    when :delete
      delete action, options
    end
  end
end