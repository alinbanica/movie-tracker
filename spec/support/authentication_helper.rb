module AuthenticationHelper
  def basic_authentication(username, password)
    request.env["HTTP_AUTHORIZATION"] = ActionController::HttpAuthentication::Basic.encode_credentials(username, password)
  end

  def token_authentication(token)
    request.env["HTTP_AUTHORIZATION"] = ActionController::HttpAuthentication::Token.encode_credentials(token)
  end
end