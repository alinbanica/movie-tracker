require 'rails_helper'

RSpec.describe WatchedMovie, type: :model do
  context 'model validation' do
    it_behaves_like 'valid_model', :watched_movie

    context 'empty fields' do
      it_behaves_like 'invalid_field', 'user_id is empty', :watched_movie do
        let(:ops) { {:user_id => ''} }
      end

      it_behaves_like 'invalid_field', 'movie_id is empty', :watched_movie do
        let(:ops) { {:movie_id => ''} }
      end

      it_behaves_like 'invalid_field', 'rating is empty', :watched_movie do
        let(:ops) { {:rating => ''} }
      end
    end

    context 'not a number' do
      it_behaves_like 'invalid_field', 'user_id is not a number', :watched_movie do
        let(:ops) { {:user_id => 'text'} }
      end

      it_behaves_like 'invalid_field', 'movie_id is not a number', :watched_movie do
        let(:ops) { {:movie_id => 'text'} }
      end

      it_behaves_like 'invalid_field', 'rating is not a number', :watched_movie do
        let(:ops) { {:rating => 'text'} }
      end
    end

    it_behaves_like 'invalid_field', 'rating is not in range', :watched_movie do
      let(:ops) { {:rating => 100} }
    end
  end
end
