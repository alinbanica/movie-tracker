require 'rails_helper'

RSpec.describe Review, type: :model do
  context 'model validation' do
    it_behaves_like 'valid_model', :review

    context 'empty fields' do
      it_behaves_like 'invalid_field', 'user_id is empty', :review do
        let(:ops) { {:user_id => ''} }
      end

      it_behaves_like 'invalid_field', 'movie_id is empty', :review do
        let(:ops) { {:movie_id => ''} }
      end

      it_behaves_like 'invalid_field', 'content is empty', :review do
        let(:ops) { {:content => ''} }
      end
    end

    context 'not a number' do
      it_behaves_like 'invalid_field', 'user_id is not a number', :watched_movie do
        let(:ops) { {:user_id => 'text'} }
      end

      it_behaves_like 'invalid_field', 'movie_id is not a number', :watched_movie do
        let(:ops) { {:movie_id => 'text'} }
      end
    end
  end
end
