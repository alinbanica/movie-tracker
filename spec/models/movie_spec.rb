require 'rails_helper'

RSpec.describe Movie, type: :model do
  context 'model validation' do
    it_behaves_like 'valid_model', :movie

    context 'empty fields' do
      it_behaves_like 'invalid_field', 'title is empty', :movie do
        let(:ops) { {:title => ''} }
      end

      it_behaves_like 'invalid_field', 'release date is empty', :movie do
        let(:ops) { {:release_date => ''} }
      end

      it_behaves_like 'invalid_field', 'runtime is empty', :movie do
        let(:ops) { {:runtime => ''} }
      end
    end

    it_behaves_like 'invalid_field', 'runtime is not a number', :movie do
        let(:ops) { {:runtime => 'text'} }
      end

    it_behaves_like 'invalid_field', 'runtime is not integer number', :movie do
        let(:ops) { {:runtime => -10} }
      end
  end
end
