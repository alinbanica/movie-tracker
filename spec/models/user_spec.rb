require 'rails_helper'

RSpec.describe User, type: :model do
  context 'model validation' do
    it_behaves_like 'valid_model', :user

    context 'empty fields' do
      it_behaves_like 'invalid_field', 'email is empty', :user do
        let(:ops) { {:email => ''} }
      end

      it_behaves_like 'invalid_field', 'password is empty', :user do
        let(:ops) { {:password => ''} }
      end

      it_behaves_like 'invalid_field', 'role is empty', :user do
        let(:ops) { {:role => ''} }
      end

      it_behaves_like 'invalid_field', 'email already exists', :user do
        let(:user) { FactoryGirl.create(:user) }
        let(:ops) { {:email => user.email} }
      end
    end
  end

  describe '#is_admin_user?' do
    it 'should be true if instance is admin user' do
      user = FactoryGirl.create(:admin)
      expect(user.is_admin_user?).to be true
    end

    it 'should be false if instance is not admin user' do
      user = FactoryGirl.create(:user)
      expect(user.is_admin_user?).to be false
    end
  end

  describe '#is_basic_user?' do
    it 'should be true if instance is basic user' do
      user = FactoryGirl.create(:user)
      expect(user.is_basic_user?).to be true
    end

    it 'should be false if instance is not basic user' do
      user = FactoryGirl.create(:admin)
      expect(user.is_basic_user?).to be false
    end
  end
end
