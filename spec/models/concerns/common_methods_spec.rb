require 'rails_helper'

RSpec.describe CommonMethods do
  before :all do
    Temping.create :common_model do
      include CommonMethods

      with_columns do |t|
        t.integer :user_id, :movie_id
        t.string :something
        t.timestamps
      end
    end

    @common = CommonModel.create!({
        :user_id => 10,
        :movie_id => 12,
        :created_at => Time.now - 10.minutes,
        :updated_at => Time.now - 5.minutes,
        :something => 'test'
      })
  end

  describe '#as_json' do
    it 'should convert created_at and updated_at fields to iso8601' do
      result = @common.as_json

      expect(result['created_at']).to eq(@common.created_at.iso8601)
      expect(result['updated_at']).to eq(@common.updated_at.iso8601)
    end
  end
end