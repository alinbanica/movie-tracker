require 'rails_helper'
require 'cancan/matchers'

RSpec.describe Ability, :type => :model do
  describe 'User abilities' do
    subject(:ability) { Ability.new(user) }
    let(:user) { nil }

    context 'as guest user' do
      let(:some_user) { FactoryGirl.create(:user) }
      let(:some_movie) { FactoryGirl.create(:movie) }
      let(:some_watched_movie) { FactoryGirl.create(:watched_movie) }
      let(:some_review) { FactoryGirl.create(:review) }

      describe 'create' do
        it { should be_able_to(:create, User.new) }
        it { should_not be_able_to(:create, Movie.new) }
        it { should_not be_able_to(:create, WatchedMovie.new) }
        it { should_not be_able_to(:create, Review.new) }
      end

      describe 'read' do
        it { should be_able_to(:read, some_user) }
        it { should be_able_to(:read, some_movie) }
        it { should be_able_to(:read, some_watched_movie) }
        it { should be_able_to(:read, some_review) }
      end

      describe 'destroy' do
        it { should_not be_able_to(:destroy, some_user) }
        it { should_not be_able_to(:destroy, some_movie) }
        it { should_not be_able_to(:destroy, some_watched_movie) }
        it { should_not be_able_to(:destroy, some_review) }
      end

      describe 'update' do
        it { should_not be_able_to(:update, some_user) }
        it { should_not be_able_to(:update, some_movie) }
        it { should_not be_able_to(:update, some_watched_movie) }
        it { should_not be_able_to(:update, some_review) }
      end
    end

    context 'as basic user' do
      let(:user) { FactoryGirl.create(:user) }
      let(:user_watched_movie) { FactoryGirl.create(:watched_movie, :user => user) }
      let(:user_review) { FactoryGirl.create(:review, :user => user) }

      let(:some_user) { FactoryGirl.create(:user) }
      let(:some_movie) { FactoryGirl.create(:movie) }
      let(:some_watched_movie) { FactoryGirl.create(:watched_movie) }
      let(:some_review) { FactoryGirl.create(:review) }

      describe 'create' do
        it { should_not be_able_to(:create, User.new) }
        it { should_not be_able_to(:create, Movie.new) }
        it { should be_able_to(:create, WatchedMovie.new) }
        it { should be_able_to(:create, Review.new) }
      end

      describe 'read' do
        it { should be_able_to(:read, some_user) }
        it { should be_able_to(:read, some_movie) }
        it { should be_able_to(:read, some_watched_movie) }
        it { should be_able_to(:read, some_review) }
      end

      describe 'destroy' do
        it { should_not be_able_to(:destroy, some_user) }
        it { should be_able_to(:destroy, user) }

        it { should_not be_able_to(:destroy, some_movie) }

        it { should_not be_able_to(:destroy, some_watched_movie) }
        it { should be_able_to(:destroy, user_watched_movie) }

        it { should_not be_able_to(:destroy, some_review) }
        it { should be_able_to(:destroy, user_review) }
      end

      describe 'update' do
        it { should_not be_able_to(:update, some_user) }
        it { should be_able_to(:update, user) }

        it { should_not be_able_to(:update, some_movie) }

        it { should_not be_able_to(:update, some_watched_movie) }
        it { should be_able_to(:update, user_watched_movie) }

        it { should_not be_able_to(:update, some_review) }
        it { should be_able_to(:update, user_review) }
      end
    end

    context 'as admin user' do
      let(:user) { FactoryGirl.create(:admin) }
      let(:user_watched_movie) { FactoryGirl.create(:watched_movie, :user => user) }
      let(:user_review) { FactoryGirl.create(:review, :user => user) }

      let(:some_user) { FactoryGirl.create(:user) }
      let(:some_movie) { FactoryGirl.create(:movie) }
      let(:some_watched_movie) { FactoryGirl.create(:watched_movie) }
      let(:some_review) { FactoryGirl.create(:review) }

      describe 'create' do
        it { should be_able_to(:create, User.new) }
        it { should be_able_to(:create, Movie.new) }
        it { should be_able_to(:create, WatchedMovie.new) }
        it { should be_able_to(:create, Review.new) }
      end

      describe 'read' do
        it { should be_able_to(:read, some_user) }
        it { should be_able_to(:read, some_movie) }
        it { should be_able_to(:read, some_watched_movie) }
        it { should be_able_to(:read, some_review) }
      end

      describe 'destroy' do
        it { should be_able_to(:destroy, some_user) }
        it { should be_able_to(:destroy, user) }

        it { should be_able_to(:destroy, some_movie) }

        it { should be_able_to(:destroy, some_watched_movie) }
        it { should be_able_to(:destroy, user_watched_movie) }

        it { should be_able_to(:destroy, some_review) }
        it { should be_able_to(:destroy, user_review) }
      end

      describe 'update' do
        it { should be_able_to(:update, some_user) }
        it { should be_able_to(:update, user) }

        it { should be_able_to(:update, some_movie) }

        it { should be_able_to(:update, some_watched_movie) }
        it { should be_able_to(:update, user_watched_movie) }

        it { should be_able_to(:update, some_review) }
        it { should be_able_to(:update, user_review) }
      end
    end
  end
end
