require 'rails_helper'
include AuthenticationHelper
include UtilsHelper
include SharedExamples

RSpec.describe Api::V1::WatchedMoviesController, :type => :controller do
  shared_examples_for 'is_require_valid_input_data' do |authenticate|
    it_behaves_like 'invalid_data_on_create', 'missing user_id', authenticate do
      let!(:opts) { default_options.reject{ |k| k == :user_id } }
    end

    it_behaves_like 'invalid_data_on_create', 'missing movie_id', authenticate do
      let!(:opts) { default_options.reject!{ |k| k == :movie_id } }
    end

    it_behaves_like 'invalid_data_on_create', 'invalid user_id', authenticate do
      let!(:opts) { default_options.merge({:user_id => 'test'}) }
    end

    it_behaves_like 'invalid_data_on_create', 'invalid movie_id', authenticate do
      let!(:opts) { default_options.merge({:movie_id => 'test'}) }
    end

    it_behaves_like 'invalid_data_on_create', 'invalid rating', authenticate do
      let!(:opts) { default_options.merge({:rating => 100}) }
    end

    it_behaves_like 'invalid_data_on_create', 'movie_already_taken', authenticate do
      let!(:another_watched_movie) { FactoryGirl.create(:watched_movie, :user => user, :movie => movie) }
      let!(:opts) { default_options.merge({ :rating => 100, :user_id => user.id, :movie_id => movie.id }) }
    end
  end

  shared_examples_for 'can_view_records_by_user' do |authenticate|
    before :each do
      FactoryGirl.create(:watched_movie,
        :user => user,
        :movie => movie2
      )

      FactoryGirl.create(:watched_movie)
    end

    it 'should list all records from a specified user' do
      opts = { :user_id => user.id }

      basic_authentication(user.email, user.password) if authenticate
      send_request(http_method, action, opts)

      json = check_success_response
      expect(json['data']).to eq(WatchedMovie.by_user(user.id).as_json)
    end

    it 'should list no records if user do not have watched movies' do
      user2 = FactoryGirl.create(:user)
      opts = { :user_id => user2.id }

      basic_authentication(user.email, user.password) if authenticate
      send_request(http_method, action, opts)

      json = check_success_response
      expect(json['data']).to be_empty
    end

    it 'should list no records if user do not exist' do
      opts = { :user_id => User.maximum(:id).to_i.next }

      basic_authentication(user.email, user.password) if authenticate
      send_request(http_method, action, opts)

      json = check_success_response
      expect(json['data']).to be_empty
    end
  end

  shared_examples_for 'cannot_update_user_and_movie_ids' do |authenticate|
    it 'should update record details' do
      old_data = WatchedMovie.find(opts[:id])

      basic_authentication(user.email, user.password) if authenticate
      send_request(http_method, action, opts)

      json = check_success_response

      expect(json['data']['user_id']).to eq(old_data.user_id)
      expect(json['data']['movie_id']).to eq(old_data.movie_id)
    end
  end

  shared_examples_for 'can_update_only_personal_record' do |authenticate|
    it_behaves_like 'can_update_record', true, WatchedMovie do
      let!(:watched_movie) { FactoryGirl.create(:watched_movie, user: user) }
      let!(:opts) { {:id => watched_movie.id, :rating => 2} }
    end

    it_behaves_like 'cannot_update_record', true, WatchedMovie do
      let!(:watched_movie) { FactoryGirl.create(:watched_movie, user: user2) }
      let!(:opts) { {:id => watched_movie.id, :rating => 2} }
    end
  end

  shared_examples_for 'can_update_any_record' do |authenticate|
    it_behaves_like 'can_update_record', true, WatchedMovie do
      let!(:watched_movie) { FactoryGirl.create(:watched_movie, user: user) }
      let!(:opts) { {:id => watched_movie.id, :rating => 2} }
    end

    it_behaves_like 'can_update_record', true, WatchedMovie do
      let!(:watched_movie) { FactoryGirl.create(:watched_movie, user: user2) }
      let!(:opts) { {:id => watched_movie.id, :rating => 2} }
    end
  end

  shared_examples_for 'can_delete_only_personal_record' do |authenticate|
    it_behaves_like 'can_delete_record', true, WatchedMovie do
      let!(:watched_movie) { FactoryGirl.create(:watched_movie, user: user) }
      let!(:opts) { {:id => watched_movie.id} }
    end

    it_behaves_like 'cannot_delete_record', true, WatchedMovie do
      let!(:watched_movie) { FactoryGirl.create(:watched_movie, user: user2) }
      let!(:opts) { {:id => watched_movie.id} }
    end
  end

  shared_examples_for 'can_delete_any_record' do |authenticate|
    it_behaves_like 'can_delete_record', true, WatchedMovie do
      let!(:watched_movie) { FactoryGirl.create(:watched_movie, user: user) }
      let!(:opts) { {:id => watched_movie.id} }
    end

    it_behaves_like 'can_delete_record', true, WatchedMovie do
      let!(:watched_movie) { FactoryGirl.create(:watched_movie, user: user2) }
      let!(:opts) { {:id => watched_movie.id} }
    end
  end

  describe 'GET /watched_movies' do
    let!(:http_method) { :get }
    let!(:action) { :index }

    let!(:user) { FactoryGirl.create(:user) }
    let!(:movie) { FactoryGirl.create(:movie) }
    let!(:movie2) { FactoryGirl.create(:movie) }

    let!(:watched_movie) { FactoryGirl.create(:watched_movie,
      :user => user,
      :movie => movie
      ) }

    let!(:opts) { {} }

    context 'guest user' do
      it_behaves_like 'is_not_require_authentication'
      it_behaves_like 'is_authorized_user'
      it_behaves_like 'can_view_all_records', false, WatchedMovie
      it_behaves_like 'can_view_records_by_user', false, WatchedMovie
    end

    context 'basic user' do
      it_behaves_like 'is_not_require_authentication'
      it_behaves_like 'is_authorized_user', true
      it_behaves_like 'can_view_all_records', true, WatchedMovie
      it_behaves_like 'can_view_records_by_user', true, WatchedMovie
    end

    context 'admin user' do
      it_behaves_like 'is_not_require_authentication'

      it_behaves_like 'is_authorized_user', true do
        let!(:user) { FactoryGirl.create(:admin) }
      end

      it_behaves_like 'can_view_all_records', true, WatchedMovie
      it_behaves_like 'can_view_records_by_user', true, WatchedMovie do
        let!(:user) { FactoryGirl.create(:admin) }
      end
    end
  end

  describe 'POST /watched_movies' do
    let!(:http_method) { :post }
    let!(:action) { :create }

    let!(:user) { FactoryGirl.create(:user) }
    let!(:movie) { FactoryGirl.create(:movie) }

    let!(:default_options) {
      {
        :user_id => user.id,
        :movie_id => movie.id,
        :rating => 8
      }
    }

    let!(:opts) { default_options }

    context 'guest user' do
      it_behaves_like 'is_require_authentication'
    end

    context 'basic user' do
      it_behaves_like 'is_require_authentication'
      it_behaves_like 'is_require_valid_input_data', true
      it_behaves_like 'is_authorized_user', true
      it_behaves_like 'can_create_new_record', true, WatchedMovie
    end

    context 'admin user' do
      it_behaves_like 'is_require_authentication'

      it_behaves_like 'is_require_valid_input_data', true do
        let!(:user) { FactoryGirl.create(:admin) }
      end

      it_behaves_like 'is_authorized_user', true do
        let!(:user) { FactoryGirl.create(:admin) }
      end

      it_behaves_like 'can_create_new_record', true, WatchedMovie do
        let!(:user) { FactoryGirl.create(:admin) }
      end
    end
  end

  describe 'GET /watched_movies/:id' do
    let!(:http_method) { :get }
    let!(:action) { :show }

    let!(:user) { FactoryGirl.create(:user) }
    let!(:movie) { FactoryGirl.create(:movie) }

    let!(:watched_movie) { FactoryGirl.create(:watched_movie,
      :user => user,
      :movie => movie
      ) }

    let!(:opts) { {:id => watched_movie.id} }

    context 'guest user' do
      it_behaves_like 'is_not_require_authentication'
      it_behaves_like 'is_require_valid_record_id', WatchedMovie
      it_behaves_like 'is_authorized_user', false
      it_behaves_like 'can_view_record_details', false , WatchedMovie
    end

    context 'basic user' do
      it_behaves_like 'is_not_require_authentication'
      it_behaves_like 'is_require_valid_record_id', WatchedMovie
      it_behaves_like 'is_authorized_user', true
      it_behaves_like 'can_view_record_details', true, WatchedMovie
    end

    context 'admin user' do
      it_behaves_like 'is_not_require_authentication'
      it_behaves_like 'is_require_valid_record_id', WatchedMovie

      it_behaves_like 'is_authorized_user' do
        let!(:user) { FactoryGirl.create(:admin) }
      end

      it_behaves_like 'can_view_record_details', true, WatchedMovie
    end
  end

  describe 'PUT /watched_movies/:id' do
    let!(:http_method) { :put }
    let!(:action) { :update }

    let!(:user) { FactoryGirl.create(:user) }
    let!(:movie) { FactoryGirl.create(:movie) }

    let!(:user2) { FactoryGirl.create(:user) }
    let!(:movie2) { FactoryGirl.create(:movie) }

    let!(:watched_movie) { FactoryGirl.create(:watched_movie,
      :user => user,
      :movie => movie
      ) }

    let!(:opts) { {:id => watched_movie.id} }

    context 'guest user' do
      it_behaves_like 'is_require_authentication'
      it_behaves_like 'is_require_valid_record_id', WatchedMovie
    end

    context 'basic user' do
      it_behaves_like 'is_require_authentication'
      it_behaves_like 'is_require_valid_record_id', WatchedMovie
      it_behaves_like 'is_authorized_user', true
      it_behaves_like 'can_update_only_personal_record', true

      it_behaves_like 'cannot_update_user_and_movie_ids', true, WatchedMovie do
        let!(:opts) { {
          :id => watched_movie.id,
          :movie_id => movie2.id,
          :user_id => user2.id,
          :rating => 10
        } }
      end
    end

    context 'admin user' do
      it_behaves_like 'is_require_authentication'
      it_behaves_like 'is_require_valid_record_id', WatchedMovie

      it_behaves_like 'is_authorized_user', true do
        let!(:user) { FactoryGirl.create(:admin) }
      end

      it_behaves_like 'can_update_any_record', true do
        let!(:user) { FactoryGirl.create(:admin) }
      end

      it_behaves_like 'cannot_update_user_and_movie_ids', true do
        let!(:user) { FactoryGirl.create(:admin) }
        let!(:opts) { {
          :id => watched_movie.id,
          :movie_id => movie2.id,
          :user_id => user2.id,
          :rating => 10
        } }
      end
    end
  end

  describe 'DELETE /watched_movies/:id' do
    let!(:http_method) { :delete }
    let!(:action) { :destroy }

    let!(:user) { FactoryGirl.create(:user) }
    let!(:movie) { FactoryGirl.create(:movie) }

    let!(:user2) { FactoryGirl.create(:user) }
    let!(:movie2) { FactoryGirl.create(:movie) }

    let!(:watched_movie) { FactoryGirl.create(:watched_movie,
      :user => user,
      :movie => movie
      ) }

    let!(:opts) { {:id => watched_movie.id} }

    context 'guest user' do
      it_behaves_like 'is_require_authentication'
      it_behaves_like 'is_require_valid_record_id', WatchedMovie
    end

    context 'basic user' do
      it_behaves_like 'is_require_authentication'
      it_behaves_like 'is_require_valid_record_id', WatchedMovie
      it_behaves_like 'is_authorized_user', true
      it_behaves_like 'can_delete_only_personal_record', true
    end

    context 'admin user' do
      it_behaves_like 'is_require_authentication'
      it_behaves_like 'is_require_valid_record_id', WatchedMovie

      it_behaves_like 'is_authorized_user', true do
        let!(:user) { FactoryGirl.create(:admin) }
      end

      it_behaves_like 'can_delete_any_record', true do
        let!(:user) { FactoryGirl.create(:admin) }
      end
    end
  end
end
