require 'rails_helper'

RSpec.describe Api::V1::MoviesController, :type => :controller do
  shared_examples_for 'is_require_valid_input_data' do |authenticate|
    it_behaves_like 'invalid_data_on_create', 'missing title', authenticate do
      let!(:opts) { default_options.reject{ |k| k == :title } }
    end

    it_behaves_like 'invalid_data_on_create', 'missing runtime', authenticate do
      let!(:opts) { default_options.reject!{ |k| k == :runtime } }
    end

    it_behaves_like 'invalid_data_on_create', 'missing release_date', authenticate do
      let!(:opts) { default_options.reject!{ |k| k == :release_date } }
    end

    it_behaves_like 'invalid_data_on_create', 'invalid runtime', authenticate do
      let!(:opts) { default_options.merge({:runtime => 'test'}) }
    end
  end

  describe 'GET /movies' do
    let!(:http_method) { :get }
    let!(:action) { :index }

    let!(:user) { FactoryGirl.create(:user) }
    let!(:movie) { FactoryGirl.create(:movie) }
    let!(:opts) { {} }

    context 'guest user' do
      it_behaves_like 'is_not_require_authentication'
      it_behaves_like 'is_authorized_user'
      it_behaves_like 'can_view_all_records', false, Movie
    end

    context 'basic user' do
      it_behaves_like 'is_not_require_authentication'
      it_behaves_like 'is_authorized_user', true
      it_behaves_like 'can_view_all_records', true, Movie
    end

    context 'admin user' do
      it_behaves_like 'is_not_require_authentication'

      it_behaves_like 'is_authorized_user', true do
        let!(:user) { FactoryGirl.create(:admin) }
      end

      it_behaves_like 'can_view_all_records', true, Movie
    end
  end

  describe 'POST /movies' do
    let!(:http_method) { :post }
    let!(:action) { :create }

    let!(:default_options) {
      {
        :title => 'Another title',
        :release_date => Time.now - 1.day,
        :runtime => 10
      }
    }

    let!(:opts) { default_options }
    let!(:user) { FactoryGirl.create(:user) }

    context 'guest user' do
      it_behaves_like 'is_require_authentication'
    end

    context 'basic user' do
      it_behaves_like 'is_require_authentication'
      it_behaves_like 'is_not_authorized_user', true
    end

    context 'admin user' do
      it_behaves_like 'is_require_authentication'
      it_behaves_like 'is_require_valid_input_data', true do
        let!(:user) { FactoryGirl.create(:admin) }
      end

      it_behaves_like 'is_authorized_user', true do
        let!(:user) { FactoryGirl.create(:admin) }
      end

      it_behaves_like 'can_create_new_record', true, Movie do
        let!(:user) { FactoryGirl.create(:admin) }
      end
    end
  end

  describe 'GET /movies/:id' do
    let!(:http_method) { :get }
    let!(:action) { :show }

    let!(:user) { FactoryGirl.create(:user) }
    let!(:movie) { FactoryGirl.create(:movie) }
    let!(:opts) { {:id => movie.id} }

    context 'guest user' do
      it_behaves_like 'is_not_require_authentication'
      it_behaves_like 'is_require_valid_record_id', Movie
      it_behaves_like 'is_authorized_user', false
      it_behaves_like 'can_view_record_details', false , Movie
    end

    context 'basic user' do
      it_behaves_like 'is_not_require_authentication'
      it_behaves_like 'is_require_valid_record_id', Movie
      it_behaves_like 'is_authorized_user', true
      it_behaves_like 'can_view_record_details', true, Movie
    end

    context 'admin user' do
      it_behaves_like 'is_not_require_authentication'
      it_behaves_like 'is_require_valid_record_id', Movie

      it_behaves_like 'is_authorized_user' do
        let!(:user) { FactoryGirl.create(:admin) }
      end

      it_behaves_like 'can_view_record_details', true, Movie
    end
  end

  describe 'PUT /movies/:id' do
    let!(:http_method) { :put }
    let!(:action) { :update }

    let!(:user) { FactoryGirl.create(:user) }
    let!(:movie) { FactoryGirl.create(:movie) }
    let!(:opts) { {:id => movie.id} }

    context 'guest user' do
      it_behaves_like 'is_require_authentication'
      it_behaves_like 'is_require_valid_record_id', Movie
    end

    context 'basic user' do
      it_behaves_like 'is_require_authentication'
      it_behaves_like 'is_require_valid_record_id', Movie
      it_behaves_like 'is_not_authorized_user', true
    end

    context 'admin user' do
      it_behaves_like 'is_require_authentication'
      it_behaves_like 'is_require_valid_record_id', Movie

      it_behaves_like 'is_authorized_user', true do
        let!(:user) { FactoryGirl.create(:admin) }
      end

      it_behaves_like 'can_update_record', true, Movie do
        let!(:user) { FactoryGirl.create(:admin) }
        let!(:opts) { {:id => movie.id, :title => 'Testing', :runtime => 10} }
      end
    end
  end

  describe 'DELETE /movies/:id' do
    let!(:http_method) { :delete }
    let!(:action) { :destroy }

    let!(:user) { FactoryGirl.create(:user) }
    let!(:movie) { FactoryGirl.create(:movie) }
    let!(:opts) { {:id => movie.id} }

    context 'guest user' do
      it_behaves_like 'is_require_authentication'
      it_behaves_like 'is_require_valid_record_id', Movie
    end

    context 'basic user' do
      it_behaves_like 'is_require_authentication'
      it_behaves_like 'is_require_valid_record_id', Movie
      it_behaves_like 'is_not_authorized_user', true
    end

    context 'admin user' do
      it_behaves_like 'is_require_authentication'
      it_behaves_like 'is_require_valid_record_id', Movie

      it_behaves_like 'is_authorized_user', true do
        let!(:user) { FactoryGirl.create(:admin) }
      end

      it_behaves_like 'can_delete_record', true do
        let!(:user) { FactoryGirl.create(:admin) }
      end
    end
  end
end
