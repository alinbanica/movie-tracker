require 'rails_helper'
include AuthenticationHelper
include UtilsHelper
include SharedExamples

RSpec.describe Api::V1::ReviewsController, :type => :controller do
  shared_examples_for 'is_require_valid_input_data' do |authenticate|
    it_behaves_like 'invalid_data_on_create', 'missing user_id', authenticate do
      let!(:opts) { default_options.reject{ |k| k == :user_id } }
    end

    it_behaves_like 'invalid_data_on_create', 'missing movie_id', authenticate do
      let!(:opts) { default_options.reject!{ |k| k == :movie_id } }
    end

    it_behaves_like 'invalid_data_on_create', 'missing content', authenticate do
      let!(:opts) { default_options.reject!{ |k| k == :content } }
    end

    it_behaves_like 'invalid_data_on_create', 'invalid user_id', authenticate do
      let!(:opts) { default_options.merge({:user_id => 'test'}) }
    end

    it_behaves_like 'invalid_data_on_create', 'invalid movie_id', authenticate do
      let!(:opts) { default_options.merge({:movie_id => 'test'}) }
    end
  end

  shared_examples_for 'can_view_records_by_user' do |authenticate|
    let!(:review2) {
      FactoryGirl.create(:review,
        :user => user,
        :movie => movie
      )
    }

    let!(:review3) { FactoryGirl.create(:review) }
    let!(:user2) { FactoryGirl.create(:user) }

    it 'should list all records from a specified user' do
      opts = { :user_id => user.id }

      basic_authentication(user.email, user.password) if authenticate
      send_request(http_method, action, opts)

      json = check_success_response
      expect(json['data']).to eq(Review.by_user(user.id).as_json)
    end

    it_behaves_like 'empty_data', 'user do not have reviews', authenticate do
      let!(:opts) { { :user_id => user2.id } }
    end

    it_behaves_like 'empty_data', 'user do not exist', authenticate do
      let!(:opts) { { :user_id => 0 } }
    end
  end

  shared_examples_for 'can_view_records_by_movie' do |authenticate|
    before :each do
      FactoryGirl.create(:review,
        :user => user,
        :movie => movie
      )

      FactoryGirl.create(:review, :movie => movie)
    end

    it 'should list all records for a specified movie' do
      opts = { :movie_id => movie.id }

      basic_authentication(user.email, user.password) if authenticate
      send_request(http_method, action, opts)

      json = check_success_response
      expect(json['data']).to eq(Review.by_movie(movie.id).as_json)
    end

    it_behaves_like 'empty_data', 'movie do not have any reviews', authenticate do
      movie2 = FactoryGirl.create(:movie)
      let(:opts) { { :movie_id => movie2.id } }
    end

    it_behaves_like 'empty_data', 'movie do not exist', authenticate do
      let(:opts) { { :movie_id => Movie.maximum(:id).to_i.next } }
    end
  end

  shared_examples_for 'can_view_records_by_user_and_movie' do |authenticate|
    before :each do
      FactoryGirl.create(:review,
        :user => user,
        :movie => movie
      )

      FactoryGirl.create(:review, :movie => movie)
    end

    it 'should list all records for a specified movie and user' do
      opts = { :movie_id => movie.id, :user_id => user.id }

      basic_authentication(user.email, user.password) if authenticate
      send_request(http_method, action, opts)

      json = check_success_response
      expect(json['data']).to eq(Review.by_user(user.id).by_movie(movie.id).as_json)
    end

    it_behaves_like 'empty_data', 'user exists but the movie do not have any reviews from the user', authenticate do
      movie2 = FactoryGirl.create(:movie)
      let!(:opts) { { :user_id => user.id, :movie_id => movie2.id } }
    end

    it_behaves_like 'empty_data', 'user exists but the movie do not exist', authenticate do
      let!(:opts) { { :user_id => user.id, :movie_id => Movie.maximum(:id).to_i.next } }
    end
  end

  shared_examples_for 'cannot_update_user_and_movie_ids' do |authenticate|
    it 'should update record details' do
      old_data = Review.find(opts[:id])

      basic_authentication(user.email, user.password) if authenticate
      send_request(http_method, action, opts)

      json = check_success_response

      expect(json['data']['user_id']).to eq(old_data.user_id)
      expect(json['data']['movie_id']).to eq(old_data.movie_id)
    end
  end

  shared_examples_for 'can_update_only_personal_record' do |authenticate|
    it_behaves_like 'can_update_record', true, Review do
      let!(:review) { FactoryGirl.create(:review, user: user) }
      let!(:opts) { {:id => review.id, :rating => 2} }
    end

    it_behaves_like 'cannot_update_record', true, Review do
      let!(:review) { FactoryGirl.create(:review, user: user2) }
      let!(:opts) { {:id => review.id, :rating => 2} }
    end
  end

  shared_examples_for 'can_update_any_record' do |authenticate|
    it_behaves_like 'can_update_record', true, Review do
      let!(:review) { FactoryGirl.create(:review, user: user) }
      let!(:opts) { {:id => review.id, :rating => 2} }
    end

    it_behaves_like 'can_update_record', true, Review do
      let!(:review) { FactoryGirl.create(:review, user: user2) }
      let!(:opts) { {:id => review.id, :rating => 2} }
    end
  end

  shared_examples_for 'can_delete_only_personal_record' do |authenticate|
    it_behaves_like 'can_delete_record', true, Review do
      let!(:review) { FactoryGirl.create(:review, user: user) }
      let!(:opts) { {:id => review.id} }
    end

    it_behaves_like 'cannot_delete_record', true, Review do
      let!(:review) { FactoryGirl.create(:review, user: user2) }
      let!(:opts) { {:id => review.id} }
    end
  end

  shared_examples_for 'can_delete_any_record' do |authenticate|
    it_behaves_like 'can_delete_record', true, Review do
      let!(:review) { FactoryGirl.create(:review, user: user) }
      let!(:opts) { {:id => review.id} }
    end

    it_behaves_like 'can_delete_record', true, Review do
      let!(:review) { FactoryGirl.create(:review, user: user2) }
      let!(:opts) { {:id => review.id} }
    end
  end

  describe 'GET /reviews' do
    let!(:http_method) { :get }
    let!(:action) { :index }

    let!(:user) { FactoryGirl.create(:user) }
    let!(:movie) { FactoryGirl.create(:movie) }

    let!(:review) {
      FactoryGirl.create(:review,
        :user => user,
        :movie => movie
      )
    }

    let!(:opts) { {} }

    context 'guest user' do
      it_behaves_like 'is_not_require_authentication'
      it_behaves_like 'is_authorized_user'
      it_behaves_like 'can_view_all_records', false, Review
      it_behaves_like 'can_view_records_by_user', false, Review
      it_behaves_like 'can_view_records_by_movie', false, Review
      it_behaves_like 'can_view_records_by_user_and_movie', false, Review
    end

    context 'basic user' do
      it_behaves_like 'is_not_require_authentication'
      it_behaves_like 'is_authorized_user', true
      it_behaves_like 'can_view_all_records', true, Review
      it_behaves_like 'can_view_records_by_user', true, Review
      it_behaves_like 'can_view_records_by_movie', true, Review
      it_behaves_like 'can_view_records_by_user_and_movie', true, Review
    end

    context 'admin user' do
      let!(:user) { FactoryGirl.create(:admin) }

      it_behaves_like 'is_not_require_authentication'
      it_behaves_like 'is_authorized_user', true
      it_behaves_like 'can_view_all_records', true, Review
      it_behaves_like 'can_view_records_by_user', true, Review
      it_behaves_like 'can_view_records_by_movie', true, Review
      it_behaves_like 'can_view_records_by_user_and_movie', true, Review
    end
  end

  describe 'POST /reviews' do
    let!(:http_method) { :post }
    let!(:action) { :create }

    let!(:user) { FactoryGirl.create(:user) }
    let!(:movie) { FactoryGirl.create(:movie) }

    let!(:default_options) {
      {
        :user_id => user.id,
        :movie_id => movie.id,
        :content => 'I like this movie'
      }
    }

    let!(:opts) { default_options }

    context 'guest user' do
      it_behaves_like 'is_require_authentication'
    end

    context 'basic user' do
      it_behaves_like 'is_require_authentication'
      it_behaves_like 'is_require_valid_input_data', true
      it_behaves_like 'is_authorized_user', true
      it_behaves_like 'can_create_new_record', true, Review
    end

    context 'admin user' do
      it_behaves_like 'is_require_authentication'

      it_behaves_like 'is_require_valid_input_data', true do
        let!(:user) { FactoryGirl.create(:admin) }
      end

      it_behaves_like 'is_authorized_user', true do
        let!(:user) { FactoryGirl.create(:admin) }
      end

      it_behaves_like 'can_create_new_record', true, Review do
        let!(:user) { FactoryGirl.create(:admin) }
      end
    end
  end

  describe 'GET /reviews/:id' do
    let!(:http_method) { :get }
    let!(:action) { :show }

    let!(:user) { FactoryGirl.create(:user) }
    let!(:movie) { FactoryGirl.create(:movie) }

    let!(:review) { FactoryGirl.create(:review,
      :user => user,
      :movie => movie
      ) }

    let!(:opts) { {:id => review.id} }

    context 'guest user' do
      it_behaves_like 'is_not_require_authentication'
      it_behaves_like 'is_require_valid_record_id', Review
      it_behaves_like 'is_authorized_user', false
      it_behaves_like 'can_view_record_details', false , Review
    end

    context 'basic user' do
      it_behaves_like 'is_not_require_authentication'
      it_behaves_like 'is_require_valid_record_id', Review
      it_behaves_like 'is_authorized_user', true
      it_behaves_like 'can_view_record_details', true, Review
    end

    context 'admin user' do
      it_behaves_like 'is_not_require_authentication'
      it_behaves_like 'is_require_valid_record_id', Review

      it_behaves_like 'is_authorized_user' do
        let!(:user) { FactoryGirl.create(:admin) }
      end

      it_behaves_like 'can_view_record_details', true, Review
    end
  end

  describe 'PUT /reviews/:id' do
    let!(:http_method) { :put }
    let!(:action) { :update }

    let!(:user) { FactoryGirl.create(:user) }
    let!(:movie) { FactoryGirl.create(:movie) }

    let!(:user2) { FactoryGirl.create(:user) }
    let!(:movie2) { FactoryGirl.create(:movie) }

    let!(:review) { FactoryGirl.create(:review,
      :user => user,
      :movie => movie
      ) }

    let!(:opts) { {:id => review.id} }

    context 'guest user' do
      it_behaves_like 'is_require_authentication'
      it_behaves_like 'is_require_valid_record_id', Review
    end

    context 'basic user' do
      it_behaves_like 'is_require_authentication'
      it_behaves_like 'is_require_valid_record_id', Review
      it_behaves_like 'is_authorized_user', true
      it_behaves_like 'can_update_only_personal_record', true

      it_behaves_like 'cannot_update_user_and_movie_ids', true, Review do
        let!(:opts) { {
          :id => review.id,
          :movie_id => movie2.id,
          :user_id => user2.id,
          :rating => 10
        } }
      end
    end

    context 'admin user' do
      it_behaves_like 'is_require_authentication'
      it_behaves_like 'is_require_valid_record_id', Review

      it_behaves_like 'is_authorized_user', true do
        let!(:user) { FactoryGirl.create(:admin) }
      end

      it_behaves_like 'can_update_any_record', true do
        let!(:user) { FactoryGirl.create(:admin) }
      end

      it_behaves_like 'cannot_update_user_and_movie_ids', true do
        let!(:user) { FactoryGirl.create(:admin) }
        let!(:opts) { {
          :id => review.id,
          :movie_id => movie2.id,
          :user_id => user2.id,
          :rating => 10
        } }
      end
    end
  end

  describe 'DELETE /reviews/:id' do
    let!(:http_method) { :delete }
    let!(:action) { :destroy }

    let!(:user) { FactoryGirl.create(:user) }
    let!(:movie) { FactoryGirl.create(:movie) }

    let!(:user2) { FactoryGirl.create(:user) }
    let!(:movie2) { FactoryGirl.create(:movie) }

    let!(:review) { FactoryGirl.create(:review,
      :user => user,
      :movie => movie
      ) }

    let!(:opts) { {:id => review.id} }

    context 'guest user' do
      it_behaves_like 'is_require_authentication'
      it_behaves_like 'is_require_valid_record_id', Review
    end

    context 'basic user' do
      it_behaves_like 'is_require_authentication'
      it_behaves_like 'is_require_valid_record_id', Review
      it_behaves_like 'is_authorized_user', true
      it_behaves_like 'can_delete_only_personal_record', true
    end

    context 'admin user' do
      it_behaves_like 'is_require_authentication'
      it_behaves_like 'is_require_valid_record_id', Review

      it_behaves_like 'is_authorized_user', true do
        let!(:user) { FactoryGirl.create(:admin) }
      end

      it_behaves_like 'can_delete_any_record', true do
        let!(:user) { FactoryGirl.create(:admin) }
      end
    end
  end
end
