require 'rails_helper'
include AuthenticationHelper
include UtilsHelper
include SharedExamples

RSpec.describe Api::V1::UsersController, :type => :controller do
  shared_examples_for "update_record_response" do
    it 'should update record details' do
      opts = {:id => user.id, :email => 'test@example.com'}

      basic_authentication(user.email, user.password)
      send_request(http_method, action, opts)

      json = check_success_response
      expect(json['data']).to eq(User.find(opts[:id]).as_json)
    end

    it 'should not update record details if email exists' do
      user2 = FactoryGirl.create(:user)
      opts = {:id => user.id, :email => user2.email }

      basic_authentication(user.email, user.password)
      send_request(http_method, action, opts)
      check_error_response(:bad_request)
    end
  end

  shared_examples_for 'is_require_valid_input_data' do
    it_behaves_like 'invalid_data_on_create', :string => 'missing email'  do
      let!(:opts) { default_options.reject{ |k| k == :email } }
    end

    it_behaves_like 'invalid_data_on_create', :string => 'missing pasword' do
      let!(:opts) { default_options.reject!{ |k| k == :password } }
    end

    it_behaves_like 'invalid_data_on_create', :string => 'missing role' do
      let!(:opts) { default_options.reject!{ |k| k == :role } }
    end

    it_behaves_like 'invalid_data_on_create', :string => 'invalid role' do
      let!(:opts) { default_options.merge({:role => 'test'}) }
    end

    it_behaves_like 'invalid_data_on_create', :string => 'duplicate email' do
      let!(:opts) {
        FactoryGirl.create(:user)
        default_options.merge({:email => User.last.email})
      }
    end
  end

  shared_examples_for 'is_authorized_user_for_create_action' do |authenticate|
    it_behaves_like 'is_authorized_user', authenticate
    it_behaves_like 'is_not_authorized_user', authenticate do
      let!(:opts) { default_options.merge({:role => User::ADMIN_USER}) }
    end
  end

  describe 'GET /users' do
    let!(:http_method) { :get }
    let!(:action) { :index }

    let!(:user) { FactoryGirl.create(:user) }
    let!(:opts) { {} }

    context 'as guest user' do
      it_behaves_like 'is_not_require_authentication'
      it_behaves_like 'is_authorized_user'
      it_behaves_like 'can_view_all_records', false, User
    end

    context 'as basic user' do
      it_behaves_like 'is_not_require_authentication'
      it_behaves_like 'is_authorized_user', true
      it_behaves_like 'can_view_all_records', true, User
    end

    context 'as admin user' do
      it_behaves_like 'is_not_require_authentication'

      it_behaves_like 'is_authorized_user', true do
        let!(:user) { FactoryGirl.create(:admin) }
      end

      it_behaves_like 'can_view_all_records', true, User
    end
  end

  describe 'POST /users' do
    let!(:http_method) { :post }
    let!(:action) { :create }

    let!(:default_options) {
      {
        :email => 'user@example.com',
        :password => 'user_1234',
        :role => User::BASIC_USER
      }
    }

    let!(:opts) { default_options }

    context 'as guest user' do
      it_behaves_like 'is_not_require_authentication'
      it_behaves_like 'is_require_valid_input_data'
      it_behaves_like 'is_authorized_user_for_create_action'
      it_behaves_like 'can_create_new_record', false, User
    end

    context 'as basic user' do
      it_behaves_like 'is_not_require_authentication'
      it_behaves_like 'is_require_valid_input_data'

      it_behaves_like 'is_authorized_user_for_create_action', true do
        let!(:user) { FactoryGirl.create(:user) }
      end

      it_behaves_like 'can_create_new_record', true, User do
        let!(:user) { FactoryGirl.create(:user) }
      end
    end

    context 'as admin user' do
      it_behaves_like 'is_not_require_authentication'
      it_behaves_like 'is_require_valid_input_data'

      it_behaves_like 'is_authorized_user_for_create_action', true do
        let!(:user) { FactoryGirl.create(:admin) }
      end

      it_behaves_like 'can_create_new_record', true, User do
        let!(:user) { FactoryGirl.create(:admin) }
      end
    end
  end

  describe 'GET /users/:id' do
    let!(:http_method) { :get }
    let!(:action) { :show }

    let!(:user) { FactoryGirl.create(:user) }
    let!(:opts) { {:id => user.id} }

    context 'as guest user' do
      it_behaves_like 'is_not_require_authentication'
      it_behaves_like 'is_require_valid_record_id', User

      it_behaves_like 'is_authorized_guest_user' do
        let!(:user2) { FactoryGirl.create(:user) }
      end

      it_behaves_like 'can_view_record_details', false, User
    end

    context 'as basic user' do
      it_behaves_like 'is_not_require_authentication'
      it_behaves_like 'is_require_valid_record_id', User

      it_behaves_like 'is_authorized_admin_user' do
        let!(:user2) { FactoryGirl.create(:user) }
      end

      it_behaves_like 'can_view_record_details', true, User
    end

    context 'as admin user' do
      it_behaves_like 'is_not_require_authentication'
      it_behaves_like 'is_require_valid_record_id', User

      it_behaves_like 'is_authorized_admin_user' do
        let!(:user) { FactoryGirl.create(:admin) }
        let!(:user2) { FactoryGirl.create(:user) }
        let!(:opts) { {:id => user.id} }
      end

      it_behaves_like 'can_view_record_details', true, User
    end
  end

  describe 'PUT /users/:id' do
    let!(:http_method) { :put }
    let!(:action) { :update }

    let!(:user) { FactoryGirl.create(:user) }
    let!(:opts) { {:id => user.id} }

    context 'as guest user' do
      it_behaves_like 'is_require_authentication'
      it_behaves_like 'is_require_valid_record_id', User
    end

    context 'as basic user' do
      it_behaves_like 'is_require_authentication'
      it_behaves_like 'is_require_valid_record_id', User

      it_behaves_like 'is_authorized_basic_user' do
        let!(:user2) { FactoryGirl.create(:user) }
      end

      it_behaves_like 'update_record_response'
    end

    context 'as admin user' do
      it_behaves_like 'is_require_authentication'
      it_behaves_like 'is_require_valid_record_id', User

      it_behaves_like 'is_authorized_admin_user' do
        let!(:user) { FactoryGirl.create(:admin) }
        let!(:user2) { FactoryGirl.create(:user) }
        let!(:opts) { {:id => user.id} }
      end

      it_behaves_like 'update_record_response' do
        let!(:user) { FactoryGirl.create(:admin) }
        let!(:opts) { {:id => user.id} }
      end
    end
  end

  describe 'DELETE /users/:id' do
    let!(:http_method) { :delete }
    let!(:action) { :destroy }

    let!(:user) { FactoryGirl.create(:user) }
    let!(:opts) { {:id => user.id} }

    context 'as guest user' do
      it_behaves_like 'is_require_authentication'
      it_behaves_like 'is_require_valid_record_id', User
    end

    context 'as basic user' do
      it_behaves_like 'is_require_authentication'
      it_behaves_like 'is_require_valid_record_id', User

      it_behaves_like 'is_authorized_basic_user' do
        let!(:user2) { FactoryGirl.create(:user) }
      end

      it_behaves_like 'can_delete_record', true
    end

    context 'as admin user' do
      it_behaves_like 'is_require_authentication'
      it_behaves_like 'is_require_valid_record_id', User

      it_behaves_like 'is_authorized_admin_user' do
        let!(:user) { FactoryGirl.create(:admin) }
        let!(:user2) { FactoryGirl.create(:user) }
        let!(:opts) { {:id => user.id} }
      end

      it_behaves_like 'can_delete_record', true do
        let!(:user) { FactoryGirl.create(:admin) }
        let!(:opts) { {:id => user.id} }
      end
    end
  end
end
