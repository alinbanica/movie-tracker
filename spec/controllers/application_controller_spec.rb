require 'rails_helper'
include AuthenticationHelper
include UtilsHelper
include SharedExamples

describe ApplicationController, :type => :controller do
  controller do
    include AuthenticationOrRequest

    def index
      render_response({}, status = :ok)
    end
  end

  describe 'rescue_from' do
    before :each do
      subject.class.skip_before_filter :authenticate_or_request
    end

    it 'should raise ActiveRecord::RecordNotFound exception' do
      def controller.index
        raise ActiveRecord::RecordNotFound
      end

      send_request(:get, :index)
      check_error_response(:not_found)
    end

    it 'should raise ActiveRecord::RecordInvalid exception' do
      def controller.index
        user = FactoryGirl.create(:user)
        raise ActiveRecord::RecordInvalid.new(user)
      end

      send_request(:get, :index)
      check_error_response(:bad_request)
    end

    it 'should raise CanCan::AccessDenied exception' do
      def controller.index
        raise CanCan::AccessDenied
      end

      send_request(:get, :index)
      check_error_response(:forbidden)
    end

    it 'should raise exception' do
      def controller.index
        raise Exception
      end

      send_request(:get, :index)
      check_error_response(:internal_server_error)
    end
  end

  describe 'authentication_or_request' do
    before :each do
      subject.class.before_filter :authenticate_or_request
    end

    it 'should request authentication' do
      send_request(:get, :index)
      check_error_response(:unauthorized)
      expect(response.headers["WWW-Authenticate"]).to eq("Basic realm=Application")
      expect(assigns(:current_user)).to be_nil
    end

    context 'basic authentication' do
      it 'should authenticate a user' do
        user = FactoryGirl.create(:user)
        basic_authentication(user.email, user.password)
        send_request(:get, :index)
        check_success_response
        expect(assigns(:current_user)).to eq(user)
      end

      it 'should not authenticate a user' do
        user = FactoryGirl.create(:user)
        basic_authentication(user.email, 'testtest')
        send_request(:get, :index)
        check_error_response(:unauthorized)
        expect(response.headers["WWW-Authenticate"]).to eq("Basic realm=Application")
      end
    end

    context 'token_authentication' do
      it 'should authenticate a user' do
        user = FactoryGirl.create(:user)
        token_authentication(user.authentication_token)
        send_request(:get, :index)
        check_success_response
        expect(assigns(:current_user)).to eq(user)
      end

      it 'should not authenticate a user' do
        user = FactoryGirl.create(:user)
        token_authentication('testtest')
        send_request(:get, :index)
        check_error_response(:unauthorized)
        expect(response.headers["WWW-Authenticate"]).to eq("Token realm=Application")
      end
    end
  end
end