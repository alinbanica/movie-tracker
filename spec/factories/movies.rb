FactoryGirl.define do
  factory :movie do
    sequence(:title) { |n| "Title#{n}" }
    storyline { 'Some Text' }
    release_date { Time.now - 1.week }
    language { 'English' }
    runtime { 120 }
  end
end
