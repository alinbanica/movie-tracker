FactoryGirl.define do
  factory :review do
    association :user
    association :movie
    content { 'Some long review' }
  end
end