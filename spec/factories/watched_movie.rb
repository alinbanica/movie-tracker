FactoryGirl.define do
  factory :watched_movie do
    association :user
    association :movie
    rating 8
  end
end