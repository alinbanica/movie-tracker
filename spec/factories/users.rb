FactoryGirl.define do
  factory :user do
    sequence(:email) { |n| "user#{n}@example.com" }
    sequence(:password) { |n| "user#{n}_1234" }
    role { User::BASIC_USER }
    sequence(:authentication_token) { |n| "user#{n}_token" }
  end

  factory :admin, parent: :user do
    role { User::ADMIN_USER }
  end
end